<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use Telegram\Bot\Keyboard\Keyboard;
use Carbon\Carbon;
use Validator;
use App\Command;
use App\Setting;
use Alert;
use Log;
use Telegram;
use DB;

class CommandController extends Controller
{
    private $client, $iduser, $clientBot, $idchat, $fname, $titleGroup, $btcprice, $ethprice, $bchprice, $btchigh, $btclow, $ethhigh, $ethlow, $bchhigh, $bchlow, $randomNum1, $randomNum2, $hasilCaptcha, $messageId, $volumeBtc, $volumeEth;
    private $luno, $indodax, $tokocrypto;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['index', 'create', 'edit']);
        $this->client = new Client(['base_uri' => 'https://www.luno.com/ajax/1/']);
        $this->clientBot = new Client(['base_uri' => 'https://api.telegram.org/bot'.env('TELEGRAM_BOT_TOKEN', 'YOUR-BOT-TOKEN').'/']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cmds = Command::paginate(5);
        return \view('commands.index', \compact('cmds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \view('commands.input');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'command'     => 'required|unique:commands|regex:/^\//',
            'message'  => 'required',
            'description'  => 'required',
            'link.*' => 'url|nullable'
        ]);

        try {
            $cmd = \trim($request->get('command'));
            $links = "[]";
            $link_titles = "[]";
            if (!is_null($request->link[0])) {
                $links = json_encode($request->get('link'));
                $link_titles = json_encode($request->get('link_title'));
            }
            $mes = Command::create([
                'command' => $cmd,
                'description' => $request->get('description'),
                'message' => $request->get('message'),
                'user_id' => Auth::user()->id,
                'links' => $links,
                'link_title' => $link_titles
            ]);

            Alert::success('Command succesfully inserted to a system', 'Yess..');

            return redirect()->route('command.index');
        } catch (\Exception $e) {
            Alert::error('Something went wrong'.$e->getMessage(), 'Oh no....');

            return redirect()->route('command.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmd = Command::findOrFail($id);

        return \view('commands.edit', \compact('cmd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'command'     => 'required|unique:commands,command,'.$id.'|regex:/^\//',
            'message'  => 'required',
            'description'  => 'required',
            'link.*' => 'url|nullable'
        ]);

        try {
            $cmd = Command::findOrFail($id);
            $cmnd = \trim($request->get('command'));
            $cmd->command = $cmnd;
            $cmd->description = $request->get('description');
            $cmd->message = $request->get('message');
            if (!is_null($request->link[0])) {
                $cmd->links = json_encode($request->get('link'));
                $cmd->link_title = json_encode($request->get('link_title'));
            } else if (is_null($request->link[0])){
                $cmd->links = "[]";
                $cmd->link_title = "[]";
            }

            $cmd->save();
            Alert::success('Command succesfully Updated', 'Yess..');

            return redirect()->route('command.index');
        } catch (\Exception $e) {
            Alert::error('Something went wrong', 'Oh no..');

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cmd = Command::findOrFail($id);
            $cmd->delete();
            Alert::success('Command succesfully deleted', 'Yess..');
        } catch (Exception $th) {
            Alert::error('Command was not succesfully deleted', 'Ooohhh...');
        }

        return redirect()->back();
    }

    public function tes()
    {
        try {
            DB::connection('sqlite')
                ->table('config')
                ->insert([
                'name' => 'filter bot',
                'value' => 'true'
            ]);
            echo 'Sqlite inserted';
        } catch (Exception $e) {
            return false;
        }
    }

    public function sendMessage($message, $btn = NULL)
    {
        if (isset($message)) {
            $txt = \strip_tags($message, '<br><a><b><i><code><pre><strong><em><br/>');
            $txt = str_replace('<br>',"\n",$txt);
            $txt = str_replace('<br />',"\n",$txt);
            $txt = html_entity_decode($txt);
            $txt = str_replace('@fname@',$this->fname,$txt);
            $txt = str_replace('@grouptitle@',$this->titleGroup,$txt);
            $txt = str_replace('@bitcoinprice@',$this->btcprice,$txt);
            $txt = str_replace('@bitcoinhigh@',$this->btchigh,$txt);
            $txt = str_replace('@bitcoinlow@',$this->btclow,$txt);
            $txt = str_replace('@ethereumprice@',$this->ethprice,$txt);
            $txt = str_replace('@ethereumhigh@',$this->ethhigh,$txt);
            $txt = str_replace('@ethereumlow@',$this->ethlow,$txt);
            $txt = str_replace('@bitcoincashprice@',$this->bchprice,$txt);
            $txt = str_replace('@bitcoincashhigh@',$this->bchhigh,$txt);
            $txt = str_replace('@bitcoincashlow@',$this->bchlow,$txt);
            $txt = str_replace('@lunobtcidrbuy@',"Rp. ".(isset($this->luno["btcidr"]["buy"]) ? $this->luno["btcidr"]["buy"] : "0"),$txt);
            $txt = str_replace('@lunobtcidrsell@',"Rp. ".(isset($this->luno["btcidr"]["sell"]) ? $this->luno["btcidr"]["sell"] : "0"),$txt);
            $txt = str_replace('@indobtcidrbuy@',"Rp. ".(isset($this->indodax["btcidr"]["buy"]) ? $this->indodax["btcidr"]["buy"] : "0"),$txt);
            $txt = str_replace('@indobtcidrsell@',"Rp. ".(isset($this->indodax["btcidr"]["sell"]) ? $this->indodax["btcidr"]["sell"] : "0"),$txt);
            // $txt = str_replace('@tokobtcidrbuy@',"Rp. ".(isset($this->tokocrypto["btcidr"]["buy"]) ? $this->tokocrypto["btcidr"]["buy"] : "0"),$txt);
            // $txt = str_replace('@tokobtcidrsell@',"Rp. ".(isset($this->tokocrypto["btcidr"]["sell"]) ? $this->tokocrypto["btcidr"]["sell"] : "0"),$txt);
            $txt = str_replace('@lunobchbtcbuy@',(isset($this->luno["bchbtc"]["buy"]) ? $this->luno["bchbtc"]["buy"] : "0"),$txt);
            $txt = str_replace('@lunobchbtcsell@',(isset($this->luno["bchbtc"]["sell"]) ? $this->luno["bchbtc"]["sell"] : "0"),$txt);
            $txt = str_replace('@indobchbtcbuy@',(isset($this->indodax["bchbtc"]["buy"]) ? $this->indodax["bchbtc"]["buy"] : "0"),$txt);
            $txt = str_replace('@indobchbtcsell@',(isset($this->indodax["bchbtc"]["sell"]) ? $this->indodax["bchbtc"]["sell"] : "0"),$txt);
            $txt = str_replace('@lunoethbtcbuy@',(isset($this->luno["ethbtc"]["buy"]) ? $this->luno["ethbtc"]["buy"] : "0"),$txt);
            $txt = str_replace('@lunoethbtcsell@',(isset($this->luno["ethbtc"]["sell"]) ? $this->luno["ethbtc"]["sell"] : "0"),$txt);
            $txt = str_replace('@indoethbtcbuy@',(isset($this->indodax["ethbtc"]["buy"]) ? $this->indodax["ethbtc"]["buy"] : "0"),$txt);
            $txt = str_replace('@indoethbtcsell@',(isset($this->indodax["ethbtc"]["sell"]) ? $this->indodax["ethbtc"]["sell"] : "0"),$txt);
            // $txt = str_replace('@tokoethbtcbuy@',(isset($this->tokocrypto["ethbtc"]["buy"]) ? $this->tokocrypto["ethbtc"]["buy"] : "0"),$txt);
            // $txt = str_replace('@tokoethbtcsell@',(isset($this->tokocrypto["ethbtc"]["sell"]) ? $this->tokocrypto["ethbtc"]["sell"] : "0"),$txt);
            // $txt = str_replace('@volumebtc@',$this->volumeBtc,$txt);
            // $txt = str_replace('@volumeeth@',$this->volumeEth,$txt);
            $txt = str_replace('@date@',$this->tanggalIndo(date('Y-m-d H:i:s')),$txt);
            if (isset($btn)) {
                $response = Telegram::sendMessage([
                    'chat_id' => $this->idchat,
                    'text' => $txt,
                    'parse_mode' => 'html',
                    'reply_markup'=>json_encode(['inline_keyboard' => $btn])
                ]);
            } else {
                $response = Telegram::sendMessage([
                    'chat_id' => $this->idchat,
                    'text' => $txt,
                    'parse_mode' => 'html'
                ]);
            }

            return $response;
        }
    }

    public function webhookUpdate()
    {
        $updates = Telegram::getWebhookUpdates();
        $cmd = $this->processMessage($updates);
        $msg = $this->getMessage($cmd);
        $btn = $this->getButton($cmd);

        return $this->sendMessage($msg,$btn);
    }

    /**
     * process message from webhook
     */
    public function processMessage($m)
    {
                if (isset($m["message"]["text"])) {
                    $sumber = $m["message"];
                    $cmd = $sumber["text"];
                    $this->iduser = $m["message"]["from"]["id"];
                    $this->idchat = $m["message"]["chat"]["id"];
                    $this->userData($sumber["chat"]["id"],$sumber["from"]["first_name"]);
                        if (isset($m["message"]["entities"])) {
                            if ( $m["message"]["entities"][0]["type"] == "bot_command") {
                                if ($m["message"]["chat"]["type"] != "private") {
                                    $usernamebot = env('TELEGRAM_BOT_USERNAME', 'NULL');
                                    if(strpos($cmd, $usernamebot) == false){
                                        return $cmd;
                                    } else{
                                        $usernamebot = str_replace("@","",$usernamebot);
                                        $cmdExplode = explode("@", $cmd);
                                        $username = $cmdExplode[1];
                                        if ($username == $usernamebot) {
                                            return $cmdExplode[0];
                                        }
                                    }
                                }
                                return $cmd;
                            }
                        }
                } elseif (isset($m["message"]["new_chat_member"])) {
                    $this->idchat = $m["message"]["chat"]["id"];
                    $this->iduser = $m["message"]["from"]["id"];
                    $sumber = $m["message"];
                    $this->userData($sumber["chat"]["id"],$sumber["new_chat_member"]["first_name"], $sumber["chat"]["title"]);
                    return $this->botFilter($m);
                }

    }

    public function getMessage($cmd)
    {
        if (isset($cmd)) {
            $message = Command::where('command', $cmd)->first();

            if ($cmd == "/infoharga" || $cmd == "/infoexchange") {
                $this->dynamicData($cmd);
            }
            if (isset($message)) {
                return $message->message;
            }
        }
    }

    public function getButton($cmd)
    {
        if (isset($cmd)) {
            $message = Command::where('command', $cmd)->first();

            if (isset($message)) {
                if (\json_decode($message->links)) {
                    $link = \json_decode($message->links);
                    $title = \json_decode($message->link_title);
                    $k = array();
                    for ($i=0; $i < count(\json_decode($message->links)); $i++) {
                        array_push($k,array(Keyboard::inlineButton(['text' => $title[$i], 'url' => $link[$i]])));
                    }

                    return $k;
                }
            }
        }
    }

    public function setEndpoint()
    {
        $this->client = new Client(['base_uri' => 'https://api.telegram.org/bot'.env('TELEGRAM_BOT_TOKEN', 'YOUR-BOT-TOKEN').'/']);
    }

    public function kickMember($iduser)
    {
            try {
                $this->setEndpoint();
                $this->client->request('POST', 'kickChatMember', [
                    'query' => [
                        'chat_id' => $this->idchat,
                        'user_id' => $iduser,
                        'until_date' => time() + (15* 60)
                    ]
                ]);
            } catch (Exception $e) {
                echo($e);
            }
    }

    public function getPrice($crpt='XBT', $cur='IDR')
    {
        $response = $this->client->request('GET', 'price_chart');

        if ($response->getStatusCode() == 200) {
            $body = (string) $response->getBody()->getContents();
            $data =  json_decode($body, true);
            $a = $data["availablePairs"];
            foreach ($a as $v ) {
                if ($v["counterCode"] == $cur && $v["baseCode"] == $crpt) {
                    return $this->formatcurrency($v["price"]);
                }
            }
        } else {
            return false;
        }
    }

    public function getHilo($crpt='XBT', $cur='IDR')
    {
        $response = $this->client->request('GET', 'charts_candles', [
            'query' => [
                'pair' => $crpt.$cur,
                'since' => strtotime("-1 day")
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $body = (string) $response->getBody()->getContents();
            $data =  json_decode($body, true);

            $high = array();
            $low = array();
            foreach ($data["candles"] as $v) {
                $high[] = $v["high"];
                $low[] = $v["low"];
            }
            $price = array(
                "High"=> $this->formatcurrency(max($high)),
                "Low"=> $this->formatcurrency(min($low)));
            return $price;
        } else {
            return false;
        }
    }

    public function getVolume($crpt='XBT', $cur='MYR')
    {
        $client = new Client();
        $response = $client->request('GET', 'https://api.mybitx.com/api/1/ticker', [
            'query' => [
                'pair' => $crpt.$cur
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $body = (string) $response->getBody()->getContents();
            $data =  json_decode($body, true);
            $a = $data["rolling_24_hour_volume"];
            return $a;
        } else {
            return false;
        }
    }

    public function getExchange($type)
    {
        $client = new Client(['base_uri' => 'http://marketprice.botlunoindonesia.website/']);

        if ($type=='bchbtc') {
            $response = $client->request('GET', $type);
            if ($response->getStatusCode() == 200) {
                $body = (string) $response->getBody()->getContents();
                $data =  json_decode($body, true);
                $a = $data[$type];
                $this->luno[$type]["market"] = $a[0]["market"];
                $this->luno[$type]["buy"] = $a[0]["buy"];
                $this->luno[$type]["sell"] = $a[0]["sell"];

                $this->indodax[$type]["market"] = $a[1]["market"];
                $this->indodax[$type]["buy"] = $a[1]["buy"];
                $this->indodax[$type]["sell"] = $a[1]["sell"];
            } else {
                return false;
            }
        } elseif ($type=='ethbtc') {
            $response = $client->request('GET', $type);
            if ($response->getStatusCode() == 200) {
                $body = (string) $response->getBody()->getContents();
                $data =  json_decode($body, true);
                $a = $data[$type];
                $this->luno[$type]["market"] = $a[0]["market"];
                $this->luno[$type]["buy"] = $a[0]["buy"];
                $this->luno[$type]["sell"] = $a[0]["sell"];

                $this->indodax[$type]["market"] = $a[1]["market"];
                $this->indodax[$type]["buy"] = $a[1]["buy"];
                $this->indodax[$type]["sell"] = $a[1]["sell"];

                // $this->tokocrypto[$type]["market"] = $a[2]["market"];
                // $this->tokocrypto[$type]["buy"] = $a[2]["buy"];
                // $this->tokocrypto[$type]["sell"] = $a[2]["sell"];
            } else {
                return false;
            }
        } elseif ($type=='btcidr') {
            $response = $client->request('GET', $type);
            if ($response->getStatusCode() == 200) {
                $body = (string) $response->getBody()->getContents();
                $data =  json_decode($body, true);
                $a = $data[$type];
                $this->luno[$type]["market"] = $a[0]["market"];
                $this->luno[$type]["buy"] = $a[0]["buy"];
                $this->luno[$type]["sell"] = $a[0]["sell"];

                $this->indodax[$type]["market"] = $a[1]["market"];
                $this->indodax[$type]["buy"] = $a[1]["buy"];
                $this->indodax[$type]["sell"] = $a[1]["sell"];

                // $this->tokocrypto[$type]["market"] = $a[2]["market"];
                // $this->tokocrypto[$type]["buy"] = $a[2]["buy"];
                // $this->tokocrypto[$type]["sell"] = $a[2]["sell"];
            } else {
                return false;
            }
        }
    }

    public function formatcurrency($floatcurr){
        return "Rp. " . number_format($floatcurr, 0, '.', ',');
    }

    public function dynamicData($cmd)
    {
        //exchange data
        if ($cmd == "/infoexchange") {
            $this->getExchange('bchbtc');
            $this->getExchange('ethbtc');
            $this->getExchange('btcidr');
        } else {
            $priceBtc = $this->getHilo();
            $priceEth = $this->getHilo('ETH');
            $priceBch = $this->getHilo('BCH');
            // $volumeBtc = $this->getVolume();
            // $volumeEth = $this->getVolume('ETH');

            $this->btcprice = $this->getPrice();
            $this->btchigh = $priceBtc["High"];
            $this->btclow = $priceBtc["Low"];

            $this->ethprice = $this->getPrice('ETH');
            $this->ethhigh = $priceEth["High"];
            $this->ethlow = $priceEth["Low"];

            $this->bchprice = $this->getPrice('BCH');
            $this->bchhigh = $priceBch["High"];
            $this->bchlow = $priceBch["Low"];

            // $this->volumeBtc = $volumeBtc;
            // $this->volumeEth = $volumeEth;
        }
    }

    public function userData($idchat, $fname, $titleGroup = NULL)
    {
        $this->idchat = $idchat;
        $this->fname = $fname;
        $this->titleGroup = $titleGroup;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editWelcomeMessage()
    {
        $cmd = Command::where('command', '@welcome')->first();

        return \view('commands.welcome-message', \compact('cmd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateWelcomeMessage(Request $request)
    {
        $this->validate($request, [
            'message'  => 'required',
            'link.*' => 'url|nullable'
        ]);

        try {
            $cmd = Command::where('command', '@welcome')->first();
            $cmd->message = $request->get('message');
            if (!is_null($request->link[0])) {
                $cmd->links = json_encode($request->get('link'));
                $cmd->link_title = json_encode($request->get('link_title'));
            } else if (is_null($request->link[0])){
                $cmd->links = NULL;
                $cmd->link_title = NULL;
            }

            $cmd->save();
            Alert::success('Command succesfully Updated', 'Yess..');

            return redirect()->route('command.index');
        } catch (\Exception $e) {
            Alert::error('Something went wrong', 'Oh no..');

            return redirect()->back();
        }
    }

    public function isFilter()
    {
        try {
            $v = Setting::where('name', 'filter bot')->select('value')->first();
            if ($v->value == 'true') {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            Log::warning("{can't check config message($e) ".date('d-M-Y H:i:s')."}");
        }
    }

    public function botFilter($m)
    {
        $idbot = env('TELEGRAM_BOT_TOKEN', 'NULL');
        $idbot = explode(":", $idbot);
        $idbot = $idbot[0];
        $i = $m["message"]["new_chat_member"]["id"];
        if ($i != $idbot) {
            if ($this->isFilter()) {
                if ($m["message"]["new_chat_member"]["is_bot"] == true) {
                    // $this->idchat = $m["message"]["chat"]["id"];
                    // $this->kickMember($m["message"]["new_chat_member"]["id"]);
                }
            }
        }
    }

    public function initSetting()
    {
        try {
            Setting::create(['name' => 'filter bot', 'value' => 'true']);
            echo 'Setting initialized';
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function tanggalIndo($date)
    {
        $date = date('Y-m-d H:i:s',strtotime($date));
        if($date == '0000-00-00')
            return 'Tanggal Kosong';

        $tgl = substr($date, 8, 2);
        $bln = substr($date, 5, 2);
        $thn = substr($date, 0, 4);
        $jam = substr($date, 11, 8);

        switch ($bln) {
            case 1 : {
                    $bln = 'Januari';
                }break;
            case 2 : {
                    $bln = 'Februari';
                }break;
            case 3 : {
                    $bln = 'Maret';
                }break;
            case 4 : {
                    $bln = 'April';
                }break;
            case 5 : {
                    $bln = 'Mei';
                }break;
            case 6 : {
                    $bln = "Juni";
                }break;
            case 7 : {
                    $bln = 'Juli';
                }break;
            case 8 : {
                    $bln = 'Agustus';
                }break;
            case 9 : {
                    $bln = 'September';
                }break;
            case 10 : {
                    $bln = 'Oktober';
                }break;
            case 11 : {
                    $bln = 'November';
                }break;
            case 12 : {
                    $bln = 'Desember';
                }break;
            default: {
                    $bln = 'UnKnown';
                }break;
        }

        $hari = date('N', strtotime($date));
        switch ($hari) {
            case 7 :
            case 0 : {
                    $hari = 'Minggu';
                }break;
            case 1 : {
                    $hari = 'Senin';
                }break;
            case 2 : {
                    $hari = 'Selasa';
                }break;
            case 3 : {
                    $hari = 'Rabu';
                }break;
            case 4 : {
                    $hari = 'Kamis';
                }break;
            case 5 : {
                    $hari = "Jum'at";
                }break;
            case 6 : {
                    $hari = 'Sabtu';
                }break;
            default: {
                    $hari = 'UnKnown';
                }break;
        }

        $tanggalIndonesia = "".$hari.", ".$tgl . " " . $bln . " " . $thn . " " . $jam;
        return $tanggalIndonesia;
    }
}
